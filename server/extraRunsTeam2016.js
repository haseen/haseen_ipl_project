const deliveriesData = require("./csvJson.js").deliveriesData;
const matchesData = require("./csvJson.js").matchesData;
const fs = require("fs");

const idList = () => {
  
  const year = "2016";
  let list = [];
  for (keys in matchesData) {
    if (matchesData[keys].season == year) list.push(matchesData[keys].id);
  }
  return list;
};

const matchId = idList(); 

function extraRunsPerTeam() {
  let res = {};

  for (keys in deliveriesData) {
    const extraRuns = deliveriesData[keys].extra_runs;
    const team = deliveriesData[keys].bowling_team;
    
    const id = deliveriesData[keys].match_id;

    
    if (team in res)
      res[team] += parseInt(extraRuns); 
    else res[team] = parseInt(extraRuns);
  }
  console.log(JSON.stringify(res));
  

  
}

extraRunsPerTeam()

