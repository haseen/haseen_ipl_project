const deliveriesData = require("./csvJson.js").deliveriesData;
const matchesData = require("./csvJson.js").matchesData;
const fs = require("fs");


const idres = () => {
  const year = "2015"; 
  let res = [];
  for (keys in matchesData) {
    if (matchesData[keys].season == year) res.push(matchesData[keys].id);
  }
  return res;
};
const matchId = idres(); 

function economicalBowlers() {

  const isIn2D = (str, column, array) => {
    for (let i = 0; i < array.length; i++) {
      if (array[i][column] == str) {
        return true;
        break;
      }
    }
    return false;
  };


  const indexOf2d = (str, column, array) => {
    for (let i = 0; i < array.length; i++) {
      if (array[i][column] == str) {
        return i;
        break;
      }
    }
    return -1;
  };

  let bowlerRuns = {};
  for (keys in deliveriesData) {
    const id = deliveriesData[keys].match_id;
    const bowler = deliveriesData[keys].bowler;
    const runsGiven = parseInt(deliveriesData[keys].total_runs);
    const overNums = deliveriesData[keys].over;


    if (!matchId.includes(id)) continue;


    if (!(bowler in bowlerRuns)) bowlerRuns[bowler] = {};


    if (!(id in bowlerRuns[bowler])) bowlerRuns[bowler][id] = {};

    if (!(overNums in bowlerRuns[bowler][id]))
      bowlerRuns[bowler][id][overNums] = runsGiven;

    else bowlerRuns[bowler][id][overNums] += runsGiven;
  }

 
  let bowlerEconomy = [];
  for (bowler in bowlerRuns) {
    let oversBowled = 0;
    let totalrunsGiven = 0;

    for (id in bowlerRuns[bowler]) {

      oversBowled += parseInt(
        Object.keys(bowlerRuns[bowler][id]).length
      );

    
      totalrunsGiven += Object.values(bowlerRuns[bowler][id]).reduce(
        (prev, curr) => {
          return prev + curr;
        },
        0
      );
    }
    bowlerEconomy.push([bowler, totalrunsGiven / oversBowled]); 
  } 

  
  bowlerEconomy.sort((a, b) => {
    if (a[1] === b[1]) return 0;
    else return a[1] < b[1] ? -1 : 1;
  });


  let result = {};
  for (let i = 0; i < 10; i++) {
    result[bowlerEconomy[i][0]] = bowlerEconomy[i][1];
  }
console.log(JSON.stringify(result))

  
}
economicalBowlers()



