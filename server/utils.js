const getRequest =(resource ,getData)=>{
    return fetch(resource)
    .then(res=> res.json())
    .then(res =>{
      
        getData(res)
    })
}

module.exports=getRequest