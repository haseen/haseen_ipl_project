const Highcharts = require("highcharts");
const getRequest =require("./utils")



const initialize =(data)=>{

  const years=Object.keys(data)
  const matches=Object.values(data)
Highcharts.chart("container", {
  title: {
    text: "Matches Per Year",
  },

  subtitle: {
    text: "Source: kaggle.com",
  },

  yAxis: {
    title: {
      text: "Number of Matches",
    },
  },

  xAxis: {
    categories:years
  },

  legend: {
    layout: "vertical",
    align: "right",
    verticalAlign: "middle",
  },

  // plotOptions: {
  //   series: {
  //     label: {
  //       connectorAllowed: false,
  //     },

  //   },
  // },

  series: [
    {
      name: "Matches",
      data:matches
    },
   
  
  ],

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 500,
        },
        chartOptions: {
          legend: {
            layout: "horizontal",
            align: "center",
            verticalAlign: "bottom",
          },
        },
      },
    ],
  },
});
}
Highcharts.chart("container", {
  // options - see https://api.highcharts.com/highcharts
});


getRequest(
  "output/matchesPerYear.json",
  initialize
);