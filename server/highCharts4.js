const Highcharts = require("highcharts");
const getRequest = require("./utils");

const initialize = (data) => {
  const bowler = Object.keys(data);
  const economy = Object.values(data);

  Highcharts.chart("container4", {
    title: {
      text: "top 10 economical bowlers",
    },

    subtitle: {
      text: "Source: kaggle.com",
    },

    yAxis: {
      title: {
        text: "bowler",
      },
    },

    xAxis: {
      categories: bowler,
    },

    legend: {
      layout: "vertical",
      align: "right",
      verticalAlign: "middle",
    },

    // plotOptions: {
    //   series: {
    //     label: {
    //       connectorAllowed: false,
    //     },
    //     pointStart: 2010,
    //   },
    // },

    series: [
      {
        name: "economy",
        data: economy,
      },
    ],

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500,
          },
          chartOptions: {
            legend: {
              layout: "horizontal",
              align: "center",
              verticalAlign: "bottom",
            },
          },
        },
      ],
    },
  });
};

getRequest(
  "output/top10EconomicalBowlers.json",
  initialize
);
