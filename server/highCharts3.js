const Highcharts = require("highcharts");
const getRequest = require("./utils");

const initialize = (data) => {
  const teams= Object.keys(data);
  const extraRuns = Object.values(data);

  Highcharts.chart("container3", {
    title: {
      text: "extra runs conceeded",
    },

    subtitle: {
      text: "Source: kaggle.com",
    },

    yAxis: {
      title: {
        text: "teams",
      },
    },

    xAxis: {
      categories: teams,
    },

    legend: {
      layout: "vertical",
      align: "right",
      verticalAlign: "middle",
    },

    // plotOptions: {
    //   series: {
    //     label: {
    //       connectorAllowed: false,
    //     },
    //     pointStart: 2010,
    //   },
    // },

    series: [
      {
        name: "extra runs",
        data: extraRuns,
      },
    ],

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500,
          },
          chartOptions: {
            legend: {
              layout: "horizontal",
              align: "center",
              verticalAlign: "bottom",
            },
          },
        },
      ],
    },
  });
};

getRequest(
  "output/extraRunsTeam2016.json",
  initialize
);
